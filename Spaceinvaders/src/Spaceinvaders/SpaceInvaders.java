package Spaceinvaders;

import java.awt.*;

import javax.swing.JFrame;
/**
 * Poczatkowa klasa gry z main method
 * @author �ukasz Leszczy�ski
 *
 */
public class SpaceInvaders {
	 Main_Menu_1 menu_1 ;
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String args[]) {
		
		Config.setLevels();
		Config.properties();
		Main_Menu_1 menu_1  = new Main_Menu_1();
		menu_1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu_1.setVisible(true);
		
		
	}
	
}
