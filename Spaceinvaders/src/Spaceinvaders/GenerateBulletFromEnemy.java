package Spaceinvaders;

import java.util.LinkedList;
import java.util.Random;
/**
 * Klasa watek) s�u�aca losowemu generowaniu pociskow przez wrogie statki
 * @author �ukasz Leszczy�ski
 *
 */
public class GenerateBulletFromEnemy implements Runnable {
	Thread t;
	int m ;
	Game g;
	Enemy enemy [] =  new Enemy[Config.TOTALENEMY];
	Random generator=new Random();
	GenerateBulletFromEnemy(Enemy enemy[]) {
	t =new Thread(this);
	t.start();
	}
	public void run() {
		try {
			for(;;) {
				Thread.sleep(1000);
				synchronized(this) {
					while(Config.isPAUSE()) {
						wait();
					}
					
				}
				
				int i=0;
				for(int j=0;(j<Config.TOTALENEMY);j++) {
					if(Game.getEnemy()[j]!=null) {
						enemy[i]=Game.getEnemy()[j];	
						i++;
					}
					
				}

				if(Config.getCOUNT_ENEMY()!=0) {
					m=generator.nextInt(Config.getCOUNT_ENEMY());
					if(Game.getBullets()[m]==null&&Config.getCOUNT_BULLET()<=3) {
						Game.setBullet(new Bullet(enemy[m].getPosition_x()+(Config.getVECHICLE_WIDTH()/4), enemy[m].getPosition_y()+Config.getVECHICLE_HEIGTH(), enemy[m].getBullet_dmg() ,enemy[m].getBullettype()), m);						
						Config.setCOUNT_BULLET(Config.getCOUNT_BULLET()+1);
					}
			}

			}
		}
		catch(InterruptedException e) {
			
		}
		
	}
	synchronized void myresume() {	
		notifyAll();
	}

}
