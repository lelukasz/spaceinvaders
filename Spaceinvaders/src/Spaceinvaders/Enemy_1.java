package Spaceinvaders;

import java.awt.Graphics;
import java.awt.Image;
/**
 * Klasa obiekyu Enemy1
 * @author �ukasz Leszczy�ski
 *
 */
public class Enemy_1 extends Enemy {
	
	Enemy_1(int health, int position_x, int position_y) {
		super(health, position_x, position_y);
		setHealth(health);
		setPosition_x(position_x);
		setPosition_y(position_y);
		setScore(Config.SCORE_FOR_ENEMY_1);
		settype(1);
		setBullettype(4);
		setBullet_dmg(Config.BULLET_DMG_ENEMY_1);
	}
	
	
}
