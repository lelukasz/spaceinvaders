package Spaceinvaders;

public class Enemy_3 extends Enemy {

	Enemy_3(int health, int position_x, int position_y) {
		super(health, position_x, position_y);
		health=Config.HEALTH_ENEMY_3;
		setPosition_x(position_x);
		setPosition_y(position_y);
		setScore(Config.SCORE_FOR_ENEMY_3);
		settype(3);
	}
	

}
