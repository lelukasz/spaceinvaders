package Spaceinvaders;

import java.awt.Graphics;
/**
 * Klasa Pojazdow
 * @author �ukasz Leszczy�ski
 *
 */
abstract public  class Vechicle {
	ImageComponent vechicle= new ImageComponent();
	private int health;
	private int position_x;
	private int position_y;
	private int bullet_dmg;
	private int score=0;
	private int type;
	private int bullettype;
	public int getBullettype() {
		return bullettype;
	}
	public void setBullettype(int bullettype) {
		this.bullettype = bullettype;
	}
	//type:  0 player ; 1-enemy_1; 2-enemy_2; 3-enemy_3
	Vechicle(int health, int position_x, int position_y){
		this.health=health;
		this.position_x=position_x;
		this.position_y=position_y;
		
	}
	public void setHealth (int dmg) {
		this.health=dmg;
	}
	public void setPosition_x(int position_x) {
		this.position_x=position_x;
	}
	public void setPosition_y(int position_y) {
		this.position_y=position_y;
	}
	public int getPosition_x() {
		return position_x;
	}
	public int getPosition_y() {
		return position_y;
	}
	public void setScore(int score) {
		this.score=score;
	}
	
	public int getScore() {
		return score;
	}
	
	public void addscore(int score) {
		this.score+=score;
		
	}
	public void settype(int type) {
		this.type=type;
	}
	public int getBullet_dmg() {
		return bullet_dmg;
	}
	public void setBullet_dmg(int bullet_dmg) {
		this.bullet_dmg = bullet_dmg;
	}
	public int gettype() {
		return type;
	}
	public int getHealth() {
		return health;
	}
	public void paint(Graphics g) {
		g.drawImage(vechicle.getImage(type), position_x,position_y,Config.getVECHICLE_WIDTH(),Config.getVECHICLE_HEIGTH(),null);
		
		
	}
}
