package Spaceinvaders;

import java.awt.Polygon;
import java.awt.Rectangle;
/**
 * Klasa (watke) sluzaca do poruszania sie pociskow od wrogich pojazdow w dol
 * @author �ukasz Leszczy�ski
 *
 */
public class MovingBulletFromEnemy implements Runnable {
	Thread t;
	Bullet bullets [] =  new Bullet[Config.TOTALENEMY];
	Player player;
	MovingBulletFromEnemy(){
		t=new Thread(this);
		t.start();
	}
	public void run() {
		try {
			for(;;) {
				synchronized(this) {
					while(Config.isPAUSE()) {
						wait();
					}				
				}
				bullets=Game.getBullets();
				for(int i=0; i<Config.TOTALENEMY;i++) {
					if(bullets[i]!=null) {					
						bullets[i].setPosition_y(Config.BULLET_STEP);
						bullets[i].setScale_y();					
						int xpoints[]={Game.getPlayer().getPosition_x(),Game.getPlayer().getPosition_x()+Config.getVECHICLE_WIDTH()/2,Game.getPlayer().getPosition_x()+Config.getVECHICLE_WIDTH()};
						int ypoints[]={Game.getPlayer().getPosition_y()+Config.getVECHICLE_HEIGTH(),Game.getPlayer().getPosition_y(),Game.getPlayer().getPosition_y()+Config.getVECHICLE_HEIGTH()};
						if(new Polygon(xpoints,ypoints,3).intersects(new Rectangle(bullets[i].getPosition_x(),bullets[i].getPosition_y(),Config.getBULLET_FROM_ENEMY_WIDTH(),Config.getBULLET_FROM_ENEMY_HEIGTH()))){
							player=Game.getPlayer();
							player.setHealth(player.getHealth()-bullets[i].getDmg());
							bullets[i]=null;
							Config.setCOUNT_BULLET(Config.getCOUNT_BULLET()-1);
							Game.setPlayer(player);
						}
						else if((bullets[i])!=null&&(bullets[i].getPosition_y()>=Config.getFRAME_HEIGTH())) {
							bullets[i]=null;
							Config.setCOUNT_BULLET(Config.getCOUNT_BULLET()-1);
						}
					}
				}
				Game.setBullets(bullets);
				
				Thread.sleep(10);
		
			}
		}
		catch(InterruptedException e) {
			
		}
		
	}
	synchronized void myresume() {	
		notifyAll();
	}
	
}
