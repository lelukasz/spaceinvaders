package Spaceinvaders;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.sound.sampled.LineListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * Klasa sluzaca do graficznego ustawienia okna sluzacego do zapisywania najlepszego wyniku
 * @author �ukasz Leszczy�ski
 *
 */
public class BestScores extends JPanel implements ActionListener{
	private JTextField nameField; //pole na nazw�	
	private JButton okButton; //przycisk zatwierdzania	
	public BestScores() {
		super();
		// ustawiamy layout
		//BoxLayout box=new BoxLayout(parent, flags);
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.CENTER;
		gridBag.setConstraints(this, constraints);
		setLayout(gridBag);		
		createComponents();
	}
	/**
	 * @return wprowadzona nazwa gracza
	 */
	public String getName() {
		return nameField.getText();
	}
	private void createComponents() {
		JLabel name = new JLabel("Brawo! Twoj wynik jest jednym z 5 najlepszych! Podaj swoj Nick: ");
		
		nameField = new JTextField();
		

		//pomocniczy panel do wprowadzania danych
		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(2, 1));
		inputPanel.add(name);
		inputPanel.add(nameField);
		
		
		//tworzymy przycisk 
		okButton = new JButton("Ok");
		okButton.addActionListener(this);
		

		//pomocniczy panel do wy�rodkowania element�w
		JPanel parentPanel = new JPanel();
		parentPanel.setLayout(new BorderLayout());
		parentPanel.add(inputPanel, BorderLayout.CENTER);
		parentPanel.add(okButton, BorderLayout.SOUTH);

		// dodajemy do g��wnego panelu
		this.add(parentPanel);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == okButton&&(getName()!=null)) {
			Config.setPLAYER_NAME(getName());
			if(getName()!=null)Config.setScores(getName());
			
			System.exit(0);
		}
		
	}
	
}