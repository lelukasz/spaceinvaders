package Spaceinvaders;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
//import java.util.Timer;

import javax.swing.Timer;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
/**
 * Glowna klasa gry. Tzn klasa ryzujaca gre i sterujaca gra
 * @author �ukasz Leszczy�ski
 *
 */
public class Game extends JPanel implements KeyListener, Runnable{

	private int enemy_amount;
	private Thread t;
	/** 
	 * Deklaracja w�tkow
	 */
	private MovingEnemy moveEnemy;
	private MovingBullet moveBullet;
	private MovingEnemyDown moveEnemydown;
	private GenerateBulletFromEnemy generatebullets;
	private MovingBulletFromEnemy moveBulletsFromEnemy;
	
	/**
	 * Tworzenie obiektu gracza
	 * Tworzenie tablicy obiektow wrogow
	 * Tworzenie tablicy obiektow pociskow
	 */
	static Player player=new Player(Config.HEALTH_PLAYER, Config.getPLAYER_X(), Config.getPLAYER_Y());
	static Enemy enemy [] =  new Enemy[Config.TOTALENEMY];
	static Bullet bullets[]=new Bullet[Config.TOTALENEMY];
	/**
	 * Konstruktor klasy Game
	 * Tworzy w�tki
	 * Wywo�uje metod� Distribution (Ustawiaj�c� wrogow w odpowiednim miejscu)
	 */
	public Game (){
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		Distribution(Config.getLEVELS()[0]);	
		t= new Thread(this);
		t.start();	
		moveEnemy=new MovingEnemy(this,enemy);
		moveBullet=new MovingBullet(this, enemy);
		moveEnemydown = new MovingEnemyDown (this, enemy);
		generatebullets=new GenerateBulletFromEnemy (enemy);
		moveBulletsFromEnemy= new MovingBulletFromEnemy ();
	}
	
	
	synchronized void myresume() {	
		notifyAll();
	}
	
	/**
	 * Metoda
	 * Ustawia pozycj� kazdego elemntu (z danych (zmienionych) z klasy Config)
	 * 
	 */
	public void setDistribution() {
		int count=0;
		int first_enemy_amount=0;
		int last_enemy_amount=0;
		Config.setDOWN_ENEMY(0);
		for(int i=0;i<4;i++) {
			for(int j=0; j<7;j++) {
				if(enemy[count]!=null) {
					Config.setDOWN_ENEMY(i);
					Config.setDOWN_ENEMY_Y(enemy[count].getPosition_y());
				}
				if(j==Config.getFIRST_ENEMY()) {
					if(enemy[count]!=null) {
						first_enemy_amount++;
					}
				}
				if(j==Config.getLAST_ENEMY()) {
					if(enemy[count]!=null) {
						last_enemy_amount++;
					}
				}
				if(count<enemy_amount) {
					if(enemy[count]==null){
						count++;							
					}
					else {
						enemy[count].setPosition_x(Config.getENEMY_X()+Config.getVECHICLE_WIDTH()*j);
						enemy[count].setPosition_y(Config.getENEMY_Y()+Config.getVECHICLE_HEIGTH()*i);
						count++;
					}
				}																			
			}		
		}
		if(first_enemy_amount==0) {
			Config.setFirstEnemy(Config.getFIRST_ENEMY()+1);	
		}
		if(last_enemy_amount==0) {
			Config.setLastEnemy(Config.getLAST_ENEMY()-1);	
		}
	}
	/**
	 * Metoda
	 *Ustawia pocz�tkowe pozycje wrogow na danym levelu
	 * 
	 * 
	 * @param level
	 */
	public void Distribution(int [][]level) {
		int count=0;
		for(int i=0;i<4;i++) {			
			for(int j=0; j<7;j++) {
				if(enemy[count]!=null) {
					Config.setDOWN_ENEMY(i);
				}
				switch(level[i][j]) {
				case 1:					
					enemy[count]=new Enemy_1(Config.HEALTH_ENEMY_1,Config.getENEMY_X()+Config.getVECHICLE_WIDTH()*j,Config.getFIRST_LINE()+Config.getVECHICLE_HEIGTH()*i);
					Config.setCOUNT_ENEMY(Config.getCOUNT_ENEMY()+1);	

					break;
				case 2:
					enemy[count]=new Enemy_2(Config.HEALTH_ENEMY_2,Config.getENEMY_X()+Config.getVECHICLE_WIDTH()*j,Config.getFIRST_LINE()+Config.getVECHICLE_HEIGTH()*i);
					Config.setCOUNT_ENEMY(Config.getCOUNT_ENEMY()+1);					
					break;				
				case 0:
					break;
				}
				if(enemy[count]!=null) {
					if(j<Config.getFIRST_ENEMY()) {
						Config.setFirstEnemy(j);	
					}
					if(j>Config.getLAST_ENEMY()) {
						Config.setLastEnemy(j);
					}
				}
				count++;
			}	
		}
		enemy_amount=count;		
	}
/**
 * Metoda sluzaca do rysowania planszy gry wraz z wszystkim je elementami
 */
	public void paintComponent(Graphics g) {
		//background
		g.setColor(Color.black);
		g.fillRect(1, 1, Config.getFRAME_WIDTH(), Config.getFRAME_HEIGTH());
		
		//player
		player.paint(g);
	
		// bullet	
		if(Config.getBULLET()) {
			g.setColor(Color.white);
			g.fillRect(Config.getBULLET_X(), Config.getBULLET_Y(), Config.getBULLET_WIDTH(), Config.getBULLET_HEIGTH());
		}
		
		//enemy
		for( int i=0;i<Config.TOTALENEMY;i++) {
			if(enemy[i]!=null)
				enemy[i].paint(g);
		}
		
		//bullet form enemy
		for( int i=0;i<Config.TOTALENEMY;i++) {
			if(bullets[i]!=null)
				bullets[i].paint(g);
		}
		
		//scores
		g.setColor(Color.white);
		g.setFont(new Font("moja", Font.BOLD, 30));
		g.drawString("scores:"+Integer.toString(Config.getSCORES()), 5, 30);
		
		//level
		g.setColor(Color.white);
		g.setFont(new Font("moja", Font.BOLD, 30));
		g.drawString("LEVEL:"+Config.getLEVEL(), Config.getFRAME_WIDTH()-150, 30);
		
		//health
		g.setColor(Color.white);
		g.fillRect(Config.getFRAME_WIDTH()/2-55, 5, 110, 30);
		g.setColor(Color.red);
		g.fillRect(Config.getFRAME_WIDTH()/2-50, 10, player.getHealth(), 20);
		g.setColor(Color.black);
		g.setFont(new Font("moja", Font.BOLD, 15));
		g.drawString(Integer.toString(player.getHealth())+"%", Config.getFRAME_WIDTH()/2-25, 25);
		g.dispose();
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {}
	
	@Override
	/**
	 * Reagowanie na wcisniecie klawiszy
	 */
	public void keyPressed(KeyEvent e) {
		//Wcisniecie strzalki w prawo (ruch pojazdu gracza w prawo)
		if(e.getKeyCode()==KeyEvent.VK_RIGHT&&!Config.getPAUSE()) {
			if(Config.getPLAYER_X() >= Config.getFRAME_WIDTH()-Config.getVECHICLE_WIDTH()) {
				Config.setPlayerX(Config.getFRAME_WIDTH()-Config.getVECHICLE_WIDTH());
			}
			else {
				moveRight();
			}
		}
		//wcisniecie stzralki w lewo (ruch pojazdu gracza w lewo)
		if(e.getKeyCode()==KeyEvent.VK_LEFT&&!Config.getPAUSE()) {
			if(Config.getPLAYER_X() <= 0) {
				Config.setPlayerX(0);
			}
			else {
				moveLeft();
			}
		}
		//wcisniecie spacji (wystrzelenie przez pojazd gracza pocisku)
		if(e.getKeyChar()==' '&&Config.getBULLET()==false&&!Config.getPAUSE()) {
			Config.setBULLET(true);
			Config.setBULLET_X(Config.getPLAYER_X()+(Config.getVECHICLE_WIDTH()/2));
			Config.setBULLET_Y(Config.getPLAYER_Y());
			Config.setSCALE_BULLET();
			
		}
		//wcisniecie klawisza p (zapauzowanie gry)
		if(e.getKeyChar()=='p'||e.getKeyChar()=='P') {
			if(Config.getPAUSE()==false) {
				Config.setPAUSE(true);				
			}
			
			else {
				Config.setPAUSE(false);
				setDistribution();
				player.setPosition_x(Config.getPLAYER_X());
				player.setPosition_y(Config.getPLAYER_Y());
				myresume();
				moveEnemy.myresume();
				moveBullet.myresume();
				moveEnemydown.myresume();
				generatebullets.myresume();
				moveBulletsFromEnemy.myresume();
			}			
		}		
	}
	/**
	 * Metoda s�u�aca zmianie pozycji pojazdu gracza w prawa strone
	 */
	public void moveRight(){
		Config.setPLAY(true);
		movePlayerRight();
		player.setPosition_x(Config.getPLAYER_X());
	}
	/**
	 * Metoda s�u�aca zmianie pozycji pojazdu gracza w lewa strone
	 */
	public void moveLeft(){
		Config.setPLAY(true);
		movePlayerLeft();
		player.setPosition_x(Config.getPLAYER_X());
	
	}
	public static void movePlayerRight() {
		
		Config.setPLAYER_X(Config.getPLAYER_X()+Config.getSTEP_PLAYER());
		Config.setRIGHT_DISTANCE(Config.getFRAME_WIDTH()-Config.getPLAYER_X());
		Config.setSCALE_PLAYER((double)Config.getRIGHT_DISTANCE()/(double)Config.getFRAME_WIDTH());
		
	}
	public static void movePlayerLeft() {
		Config.setPLAYER_X(Config.getPLAYER_X()-Config.getSTEP_PLAYER());

		
		Config.setRIGHT_DISTANCE(Config.getFRAME_WIDTH()-Config.getPLAYER_X());
		Config.setSCALE_PLAYER((double)Config.getRIGHT_DISTANCE()/(double)Config.getFRAME_WIDTH());
	}
	/**
	 * Obsluga watku
	 */
	public void run() {	
		try {
				for(;;) {	
					
					repaint();
					if(player.getHealth()==0)
					{
						Config.setEND_GAME(true);
					}
					
					if(Config.getCOUNT_ENEMY()!=0&&Config.isEND_GAME()==false) {
						setDistribution();
						player.setPosition_x(Config.getPLAYER_X());
						player.setPosition_y(Config.getPLAYER_Y());
						
						repaint();
					}
					//konczenie gry gdy gracz zostal zniszcozny (poziom zycia wynosi 0)
					else if(Config.isEND_GAME()==true) {
						for(int i=0; i<Config.TOTALENEMY;i++) {
							bullets[i]=null;
						}
						if(!Config.checkScores()) {
							JOptionPane.showMessageDialog(this, "Koniec gry");
							System.exit(0);
						}

						Main_Menu_1.game.setVisible(false);

						EndGame();
						synchronized(this) {
							
							wait();
											
						}				
						Config.setPAUSE(true);

					}
					//konczenie gry po przejsciu wszystkich leveli
					else if(Config.getLEVEL()==Config.LEVELS_AMOUNT){
						Config.setEND_GAME(true);
						for(int i=0; i<Config.TOTALENEMY;i++) {
							bullets[i]=null;
						}
						
						if(!Config.checkScores()) {
							JOptionPane.showMessageDialog(this, "Koniec gry");
							System.exit(0);
						}
						Main_Menu_1.game.setVisible(false);
						EndGame();
						synchronized(this) {
							
							wait();
											
						}				
						Config.setPAUSE(true);
						
						
					}
					//przejscie do kolejnego levelu
					else {
						for(int i=0; i<Config.TOTALENEMY;i++) {
							bullets[i]=null;
						}
						repaint();
						removeBullets();
						Config.setCOUNT_BULLET(0);
						Config.setPAUSE(true);
						Thread.sleep(5000);
						Config.setPAUSE(false);
						myresume();
						moveEnemy.myresume();
						moveBullet.myresume();
						moveEnemydown.myresume();
						generatebullets.myresume();
						moveBulletsFromEnemy.myresume();
						Config.setLEVEL(Config.getLEVEL()+1);
						Config.setStartConfig();
						Distribution(Config.getLEVELS()[Config.getLEVEL()-1]);

					}
					Thread.sleep(10);
				}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	static public Enemy[] getEnemy() {
		return enemy;
	}

	static public Bullet[] getBullets() {
		return bullets;
	}
	public void removeBullets() {
		for(int i=0; i<=Config.getCOUNT_BULLET();i++) {
			bullets[i]=null;
		}
	}
	static public void setBullet(Bullet bullet, int i) {
	bullets[i] = bullet;
	bullets[i].setScale_y();

	}
	static public void setBullets(Bullet bullet[]) {
		bullets = bullet;
			
		}

	static public Player getPlayer() {
		return player;
	}

	static public void setPlayer(Player playe) {
		player = playe;
	}
	static public void setBulletsPositons(){
		for(int i=0;i<Config.TOTALENEMY;i++) {
			if(bullets[i]!=null)
				
			bullets[i].setPosition_x((int)(bullets[i].getScale()*Config.getFRAME_WIDTH()));
		}
		
	}
	static public void setBulletsPositons_y(){
		for(int i=0;i<Config.TOTALENEMY;i++) {
			if(bullets[i]!=null) {
				
			bullets[i].setPosition_y2((int)(bullets[i].getScale_y()*Config.getFRAME_HEIGTH()));
			
			}
			}
		
	}
	public void EndGame() {
		GameFrame frame=new GameFrame();
	}
	

}
