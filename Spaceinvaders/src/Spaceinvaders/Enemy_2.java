package Spaceinvaders;

public class Enemy_2 extends Enemy {

	Enemy_2(int health, int position_x, int position_y) {
		super(health, position_x, position_y);
		setHealth(Config.HEALTH_ENEMY_2);
		setPosition_x(position_x);
		setPosition_y(position_y);
		setScore(Config.SCORE_FOR_ENEMY_2);
		settype(2);
		setBullettype(5);
		setBullet_dmg(Config.BULLET_DMG_ENEMY_2);
		setHealth(Config.HEALTH_ENEMY_2);
	}

}
