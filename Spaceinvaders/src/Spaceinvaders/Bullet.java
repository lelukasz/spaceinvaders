package Spaceinvaders;

import java.awt.Graphics;
/**
 * Obiekt pocisku
 * @author �ukasz Leszczy�ski
 *
 */
public class Bullet {
	ImageComponent bullet= new ImageComponent();
	private int position_x;
	private int position_y;
	private int dmg;
	private int type;
	private double scale;
	private double scale_y;
	Bullet(int x, int y, int dmg,int type){
		position_x=x;
		position_y=y;
		setScale_y();
		this.dmg=dmg;
		this.type=type;	
		setScale();

	}
	public void paint(Graphics g) {
		g.drawImage(bullet.getImage(type), position_x,position_y,Config.getBULLET_FROM_ENEMY_WIDTH(),Config.getBULLET_FROM_ENEMY_HEIGTH(),null);
		
		
	}
	public void setScale_y() {
		scale_y=(double)position_y/(double)Config.getFRAME_HEIGTH();
	}
	public double getScale_y() {
		return scale_y;
	}
	public double getScale() {
		return scale;
	}
	public void setScale() {
		scale=(double)position_x/(double)Config.getFRAME_WIDTH();
	}
	public int getPosition_x() {
		return position_x;
	}
	public void setPosition_x(int position_x) {
		this.position_x = position_x;
	}
	public int getPosition_y() {
		return position_y;
	}
	public void setPosition_y(int change) {
		this.position_y += change;
	}
	public void setPosition_y2(int position) {
		this.position_y =position;
	}
	public int getDmg() {
		return dmg;
	}
	public void setDmg(int dmg) {
		this.dmg = dmg;
	}
	
	
}
