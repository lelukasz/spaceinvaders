package Spaceinvaders;
/**
 * Klasa (watek) wluzacy do poruszania sie wrogow w poziomie
 * @author �ukasz Leszczy�ski
 *
 */
public class MovingEnemy implements Runnable {
	Thread t;
	private boolean changemove=false;
	Enemy enemy [] =  new Enemy[Config.TOTALENEMY];
	int count=0; //zlicza ilosc odbic od prawej krawedzi	
	MovingEnemy(Game g, Enemy enemy[]){
		t= new Thread(this);
		this.enemy=enemy;
		t.start();			
		}
	public void run() {
		try {		
			for(;;) {				
				synchronized(this) {
					while(Config.isPAUSE()) {
						wait();
					}
					
				}
			if(count==2&&Config.getMOVING_ENEMY_LEVEL_TIME()>3) {
				Config.setMOVING_ENEMY_LEVEL_TIME(Config.getMOVING_ENEMY_LEVEL_TIME()-1);
				count=0;
			}
			if((Config.getENEMY_X()+((Config.getLAST_ENEMY()+1)*Config.getVECHICLE_WIDTH()))>=Config.getFRAME_WIDTH()) {
				changemove=true;
				count++;
				enemy_moveLeft();
			}
			else if((Config.getENEMY_X()+(Config.getFIRST_ENEMY()*Config.getVECHICLE_WIDTH()))<0) {
				changemove=false;
				enemy_moveRight();
			}
			else if(changemove) {
				enemy_moveLeft();
			}
			else {
				enemy_moveRight();
			}			
			Thread.sleep(Config.getMOVING_ENEMY_LEVEL_TIME());
			}

			}
		catch(InterruptedException e) {
			
		}
		
		                 
	}

public void enemy_moveRight(){
		
		for( int i=0;i<Config.TOTALENEMY;i++) {
			if(enemy[i]!=null) {			
				enemy[i].setPosition_x((enemy[i].getPosition_x())+1);
			}
			
		}
		Config.setENEMY_X(Config.getENEMY_X()+1);
		Config.setEnemyX(Config.getENEMY_X());
	}
	

	public void enemy_moveLeft(){
		for( int i=0;i<Config.TOTALENEMY;i++) {
			if(enemy[i]!=null) {
			
		enemy[i].setPosition_x(enemy[i].getPosition_x()-1);
			}
			
		}
		Config.setENEMY_X(Config.getENEMY_X()-1);
		Config.setEnemyX(Config.getENEMY_X());
	
	}
	
synchronized void myresume() {
		
		
		notifyAll();
	}
}