package Spaceinvaders;
import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.IllegalStateException;


// test gita
 /**
  * Klasa przechowuj�ca i zmieniaj�ca usatwienia w grze
  * @author �ukasz Leszczy�ski
  *
  */
public class Config {
	
	//Domy�lne ustawienia leveli i zmiennych pobieranych z plikow tekstowych 
	//na wypadke gdyby dane z pliku sie nie zgadzaly
	private static int [][] defaultLevel= {{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};
	private static int defaultValue = 0;
	//Wymiary tablicy wrogich pojazdow 4x7
	final public static int COLUMNS =7;
	final public static int LINES = 4;
	//liczba wrogich pojazdow
	final public static int TOTALENEMY= COLUMNS*LINES;
	//Liczba leveli pobrana z pliku tekstowego
	final public static int LEVELS_AMOUNT=Config.setValue("#LEVELS_AMOUNT");
	
	private static int [][][]LEVELS;
	
	//Dane pobrane z pliku tekstowego (niezmienne)
	final public static int HEALTH_PLAYER= Config.setValue("#HEALTH_PLAYER");
	final public static int HEALTH_ENEMY_1= Config.setValue("#HEALTH_ENEMY_1");
	final public static int HEALTH_ENEMY_2= Config.setValue("#HEALTH_ENEMY_2");
	final public static int HEALTH_ENEMY_3= Config.setValue("#HEALTH_ENEMY_3");
	final public static int BULLET_DMG_PLAYER= Config.setValue("#BULLET_DMG_PLAYER");
	final public static int BULLET_DMG_ENEMY_1= Config.setValue("#BULLET_DMG_ENEMY_1");
	final public static int BULLET_DMG_ENEMY_2= Config.setValue("#BULLET_DMG_ENEMY_2");
	final public static int BULLET_DMG_ENEMY_3= Config.setValue("#BULLET_DMG_ENEMY_3");
	final public static int SCORE_FOR_ENEMY_1= Config.setValue("#SCORES_FOR_ENEMY_1");
	final public static int SCORE_FOR_ENEMY_2= Config.setValue("#SCORES_FOR_ENEMY_2");
	final public static int SCORE_FOR_ENEMY_3= Config.setValue("#SCORES_FOR_ENEMY_3");
	
	
	final public static int GRID_X=11;
	final public static int GRID_Y=11;
	
	private static String PLAYER_NAME;
	
	private static String[] BEST_SCORES_NAMES;
	private static int [] BEST_SCORES;
	
	private static File f=new File("src//BestScores.properties");
	final static Properties BS =new Properties();
	
	private static int []SCALE_BULLETS;
	private static int MOVING_ENEMY_DOWN_TIME=(300);
	private static int MOVING_ENEMY_LEVEL_TIME=9;
	//Wymiary okna:
	private static int FRAME_HEIGTH=800;
	private static int FRAME_WIDTH=1000;
	//Punkty gracza:
	private static int SCORES=0;
	// Level gracza
	private static int LEVEL=1;
	// Wysokosci i szerokoscpojazdow
	private static int VECHICLE_WIDTH=FRAME_WIDTH/GRID_X;
	private static int VECHICLE_HEIGTH=FRAME_HEIGTH/GRID_Y;
	//Wspolrzedne pierwszego wroga (enemy[0][0]):
	private static int ENEMY_X=2*VECHICLE_WIDTH;
	private static int ENEMY_Y=2*VECHICLE_HEIGTH;
	//Wspolrzedne statku gracza:
	private static int PLAYER_X=FRAME_WIDTH/2-(VECHICLE_WIDTH/2);
	private static int PLAYER_Y=FRAME_HEIGTH-VECHICLE_HEIGTH-36;
	// krok poruszania sie pojazdu po wcisnieciu strzalki
	private static int STEP_PLAYER=FRAME_WIDTH/50;
	
	final static int BULLET_STEP=2;
	//private static final String[] BEST_SCORES_NAME = null;
	static InputStream input = null;
	// Odleglosc wspolrzednej (x) gracza od prawej krawedzi okna (potrzebna do skalowania)
	private static int RIGHT_DISTANCE=FRAME_WIDTH-PLAYER_X; //odleg�o�c playera od prawej krawedzi
	//Skala gracza (do skalowania)
	private static double SCALE_PLAYER=(double)RIGHT_DISTANCE/(double)FRAME_WIDTH;
	// (0-6) numer kolumny skrajnego lewego wroga
	private static int FIRST_ENEMY=3; 
	//(0-6) numer kolumny skrajnego prawego wroga
	private static int LAST_ENEMY=3; 
	//Skala wrogow
	private static double SCALE_ENEMY=(double)ENEMY_X/(double)FRAME_WIDTH;
	private static double SCALE_ENEMY_Y=(double)ENEMY_Y/(double)FRAME_HEIGTH;
	
	//Wysokosc i szerokosc pocisku
	private static int BULLET_WIDTH=FRAME_WIDTH/200;
	private static int BULLET_HEIGTH=FRAME_HEIGTH/80;
	private static int BULLET_FROM_ENEMY_WIDTH=FRAME_WIDTH/20;
	private static int BULLET_FROM_ENEMY_HEIGTH=FRAME_HEIGTH/20;
	//Wspolrzedna y pierwszego wiersza worgow
	private static int FIRST_LINE=2*VECHICLE_HEIGTH;
	//Pauza gry
	private static  boolean PAUSE =false;
	//
	private static boolean PLAY=true;
	//Istnienie pocisku gracza
	private static boolean BULLET=false;
	//Wspolrzedne pocisku gracza
	private static int BULLET_X=0;
	private static int BULLET_Y=0;
	private static double SCALE_BULLET= (double)BULLET_X/(double)FRAME_WIDTH;
	private static double SCALE_BULLET_Y=(double)BULLET_Y/(double)FRAME_HEIGTH;
	//Liczba istniejacych wrogow na mapie
	private static int COUNT_ENEMY=0;
	
	
	private static int COUNT_BULLET=0;
	// Czas w jakim wrogowie poruszaja sie w dol planszy
	private static int TIME_MOVING_ENEMY_DOWN=500;
	// Koniec gry (
	private static boolean END_GAME=false;
	//
	private static int DOWN_ENEMY=0;
	//Wspolrzedna y najnierzej polorzonego ( wobecnej chwili) gracza
	private static int DOWN_ENEMY_Y=0;
	// Getters and Setters
	
	public static double getSCALE_ENEMY_Y() {
		return SCALE_ENEMY_Y;
	}
	public static double getSCALE_BULLET_Y() {
		return SCALE_BULLET_Y;
	}
	public static void setSCALE_BULLET_Y() {
		SCALE_BULLET_Y=(double)BULLET_Y/(double)FRAME_HEIGTH;;
	}
	public static String getPLAYER_NAME() {
		return PLAYER_NAME;
	}
	public static void setPLAYER_NAME(String pLAYER_NAME) {
		PLAYER_NAME = pLAYER_NAME;
	}
	public static int getMOVING_ENEMY_LEVEL_TIME() {
		return MOVING_ENEMY_LEVEL_TIME;
	}
	public static void setMOVING_ENEMY_LEVEL_TIME(int mOVING_ENEMY_LEVEL_TIME) {
		MOVING_ENEMY_LEVEL_TIME = mOVING_ENEMY_LEVEL_TIME;
	}
	public static void setSCALE_ENEMY_Y() {
		SCALE_ENEMY_Y=(double)ENEMY_Y/(double)FRAME_HEIGTH;
	}

	public static int getMOVING_ENEMY_DOWN_TIME() {
		return MOVING_ENEMY_DOWN_TIME;
	}
	public static void setMOVING_ENEMY_DOWN_TIME(int mOVING_ENEMY_DOWN_TIME) {
		MOVING_ENEMY_DOWN_TIME = mOVING_ENEMY_DOWN_TIME;
	}

	public static int getCOUNT_BULLET() {
		return COUNT_BULLET;
	}
	public static int getBULLET_FROM_ENEMY_WIDTH() {
		return BULLET_FROM_ENEMY_WIDTH;
	}
	public static void setBULLET_FROM_ENEMY_WIDTH(int bULLET_FROM_ENEMY_WIDTH) {
		BULLET_FROM_ENEMY_WIDTH = bULLET_FROM_ENEMY_WIDTH;
	}
	public static int getBULLET_FROM_ENEMY_HEIGTH() {
		return BULLET_FROM_ENEMY_HEIGTH;
	}
	public static void setBULLET_FROM_ENEMY_HEIGTH(int bULLET_FROM_ENEMY_HEIGTH) {
		BULLET_FROM_ENEMY_HEIGTH = bULLET_FROM_ENEMY_HEIGTH;
	}
	public static void setCOUNT_BULLET(int cOUNT_BULLET) {
		COUNT_BULLET = cOUNT_BULLET;
	}
	public static boolean getBULLET() {
		return BULLET;
	}
	public static boolean getPAUSE() {
		return PAUSE;
	}
	
	public static int getFRAME_HEIGTH() {
		return FRAME_HEIGTH;
	}
	public static void setFRAME_HEIGTH(int fRAME_HEIGTH) {
		FRAME_HEIGTH = fRAME_HEIGTH;
	}
	public static int getFRAME_WIDTH() {
		return FRAME_WIDTH;
	}
	public static void setFRAME_WIDTH(int fRAME_WIDTH) {
		FRAME_WIDTH = fRAME_WIDTH;
	}
	public static int getSCORES() {
		return SCORES;
	}
	public static void setSCORES(int sCORES) {
		SCORES = sCORES;
	}
	public static int getLEVEL() {
		return LEVEL;
	}
	public static void setLEVEL(int lEVEL) {
		LEVEL = lEVEL;
	}
	public static int getVECHICLE_WIDTH() {
		return VECHICLE_WIDTH;
	}
	public static void setVECHICLE_WIDTH(int vECHICLE_WIDTH) {
		VECHICLE_WIDTH = vECHICLE_WIDTH;
	}
	public static int getVECHICLE_HEIGTH() {
		return VECHICLE_HEIGTH;
	}
	public static void setVECHICLE_HEIGTH(int vECHICLE_HEIGTH) {
		VECHICLE_HEIGTH = vECHICLE_HEIGTH;
	}
	public static int getENEMY_X() {
		return ENEMY_X;
	}
	public static void setENEMY_X(int eNEMY_X) {
		ENEMY_X = eNEMY_X;
	}
	public static int getENEMY_Y() {
		return ENEMY_Y;
	}
	public static void setENEMY_Y(int eNEMY_Y) {
		ENEMY_Y = eNEMY_Y;
	}
	public static int getPLAYER_X() {
		return PLAYER_X;
	}
	public static void setPLAYER_X(int pLAYER_X) {
		PLAYER_X = pLAYER_X;
	}
	public static int getPLAYER_Y() {
		return PLAYER_Y;
	}
	public static void setPLAYER_Y(int pLAYER_Y) {
		PLAYER_Y = pLAYER_Y;
	}
	public static int getSTEP_PLAYER() {
		return STEP_PLAYER;
	}
	public static void setSTEP_PLAYER(int sTEP_PLAYER) {
		STEP_PLAYER = sTEP_PLAYER;
	}
	public static int getRIGHT_DISTANCE() {
		return RIGHT_DISTANCE;
	}
	public static void setRIGHT_DISTANCE(int rIGHT_DISTANCE) {
		RIGHT_DISTANCE = rIGHT_DISTANCE;
	}
	public static double getSCALE_PLAYER() {
		return SCALE_PLAYER;
	}
	public static void setSCALE_PLAYER(double sCALE_PLAYER) {
		SCALE_PLAYER = sCALE_PLAYER;
	}
	public static int getFIRST_ENEMY() {
		return FIRST_ENEMY;
	}
	public static void setFIRST_ENEMY(int fIRST_ENEMY) {
		FIRST_ENEMY = fIRST_ENEMY;
	}
	public static int getLAST_ENEMY() {
		return LAST_ENEMY;
	}
	public static void setLAST_ENEMY(int lAST_ENEMY) {
		LAST_ENEMY = lAST_ENEMY;
	}
	public static double getSCALE_ENEMY() {
		return SCALE_ENEMY;
	}
	public static void setSCALE_ENEMY(double sCALE_ENEMY) {
		SCALE_ENEMY = sCALE_ENEMY;
	}
	public static int getBULLET_WIDTH() {
		return BULLET_WIDTH;
	}
	public static void setBULLET_WIDTH(int bULLET_WIDTH) {
		BULLET_WIDTH = bULLET_WIDTH;
	}
	public static int getBULLET_HEIGTH() {
		return BULLET_HEIGTH;
	}
	public static void setBULLET_HEIGTH(int bULLET_HEIGTH) {
		BULLET_HEIGTH = bULLET_HEIGTH;
	}
	public static int getFIRST_LINE() {
		return FIRST_LINE;
	}
	public static void setFIRST_LINE(int fIRST_LINE) {
		FIRST_LINE = fIRST_LINE;
	}
	public static boolean isPAUSE() {
		return PAUSE;
	}
	public static void setPAUSE(boolean pAUSE) {
		PAUSE = pAUSE;
	}
	public static boolean isPLAY() {
		return PLAY;
	}
	public static void setPLAY(boolean pLAY) {
		PLAY = pLAY;
	}
	public static boolean isBULLET() {
		return BULLET;
	}
	public static void setBULLET(boolean bULLET) {
		BULLET = bULLET;
	}
	public static int getBULLET_X() {
		return BULLET_X;
	}
	public static void setBULLET_X(int bULLET_X) {
		BULLET_X = bULLET_X;
	}
	public static int getBULLET_Y() {
		return BULLET_Y;
	}
	public static void setBULLET_Y(int bULLET_Y) {
		BULLET_Y = bULLET_Y;
	}
	public static int getCOUNT_ENEMY() {
		return COUNT_ENEMY;
	}
	public static void setCOUNT_ENEMY(int cOUNT_ENEMY) {
		COUNT_ENEMY = cOUNT_ENEMY;
	}
	public static int getTIME_MOVING_ENEMY_DOWN() {
		return TIME_MOVING_ENEMY_DOWN;
	}
	public static void setTIME_MOVING_ENEMY_DOWN(int tIME_MOVING_ENEMY_DOWN) {
		TIME_MOVING_ENEMY_DOWN = tIME_MOVING_ENEMY_DOWN;
	}
	public static boolean isEND_GAME() {
		return END_GAME;
	}
	public static void setEND_GAME(boolean eND_GAME) {
		END_GAME = eND_GAME;
	}
	public static int getDOWN_ENEMY() {
		return DOWN_ENEMY;
	}
	public static void setDOWN_ENEMY(int dOWN_ENEMY) {
		DOWN_ENEMY = dOWN_ENEMY;
	}
	public static int getDOWN_ENEMY_Y() {
		return DOWN_ENEMY_Y;
	}
	public static void setDOWN_ENEMY_Y(int dOWN_ENEMY_Y) {
		DOWN_ENEMY_Y = dOWN_ENEMY_Y;
	}
	public static int getLines() {
		return LINES;
	}
	public static void setSCALE_ENEMYY(int y) {
		SCALE_ENEMY_Y=(double)ENEMY_Y/(double)y;
	}
	
	public static int[] getSCALE_BULLETS() {
		return SCALE_BULLETS;
	}
	public static void setSCALE_BULLETS(int[] sCALE_BULLETS) {
		SCALE_BULLETS = sCALE_BULLETS;
	}
	/**
	 * metoda pobieraj�ca list� najlepszych wynikow
	 */
	public static void properties() {
		BEST_SCORES=new int [5] ;
		BEST_SCORES_NAMES =new String[5];
		try {
			InputStream input = null;
			input = new FileInputStream("src//BestScores.properties");
			BS.load(input);
			for(int i=0;i<5;i++) {
				String temp=String.valueOf(1+i);
				BEST_SCORES_NAMES[i]=BS.getProperty(temp+"_name");
				BEST_SCORES[i]=Integer.parseInt(BS.getProperty(temp+"_scores"));				
			}			
		}catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * Metoda zapisuj�ca dane do pliku z najlepszymi wynikami 
	 * @param key nazwa parametru do zapisania
	 * @param value wartosc parametru do zapisania
	 */
	public static void saveProperties(String key, String value){
	    OutputStream os;
	    try {
	        os = new FileOutputStream(f);
	        BS.setProperty(key, value);
	        BS.store(os, null);
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}	    
	/**
	 * Metoda sprawdzajaca czy uzyskany przez gracza wynik zosatnie zapisany do najlepszych wynikow
	 * @return wartosc tru lu false w zaleznosci od wyniku gracza
	 */
	public static boolean checkScores() {	
		boolean check=false;
		for(int i=0;i<5;i++) {
			if(Config.getSCORES() >=BEST_SCORES[i]&&Config.getSCORES()!=0) {
				check=true;				
			}			
		}
		return check;				
	}
	/**
	 * Metoda sluzaca do zapisania wyniku gracza do tabeli z najlepszymi wynikami
	 * oraz do modyfikacji tej tabeli (usuniecie ostatniego najlepszego wyniku)
	 * @param name nick wpisany przez gracza
	 */
	public static void setScores(String name) {
		int []temp_scores=BEST_SCORES;
		String []temp_names=BEST_SCORES_NAMES;
		boolean check=false;
		for(int i=0;i<5&&!check;i++) {
			if(Config.SCORES >BEST_SCORES[i]) {
				check=true;
				BEST_SCORES[i]=Config.SCORES;
				BEST_SCORES_NAMES[i]=name;
				for(int j=i+1;j<5;j++) {
					BEST_SCORES[j]=temp_scores[j-1];
					BEST_SCORES_NAMES[j]=temp_names[j-1];
				}
			}
			else if(Config.SCORES==BEST_SCORES[i]) {
				check=true;
				if(Integer.parseInt(name)>Integer.parseInt(BEST_SCORES_NAMES[i])) {
					BEST_SCORES[i]=Config.SCORES;
					BEST_SCORES_NAMES[i]=name;
					for(int j=i+1;j<5;j++) {
						BEST_SCORES[j]=temp_scores[j-1];
						BEST_SCORES_NAMES[j]=temp_names[j-1];
					}
				}
				else {
					i--;
					BEST_SCORES[i]=Config.SCORES;
					BEST_SCORES_NAMES[i]=name;
					for(int j=i+1;j<5;j++) {
						BEST_SCORES[j]=temp_scores[j-1];
						BEST_SCORES_NAMES[j]=temp_names[j-1];
					}
				}
			}
			String temp=String.valueOf(i+1);
			saveProperties(temp+"_name",BEST_SCORES_NAMES[i]);
			saveProperties(temp+"_scores",String.valueOf(BEST_SCORES[i]));
		}
		
	}
	/**
	 * Metoda z�u�aca do ustawienia odpowiednich danych po rozpoczeciu nowego levelu
	 */
	public static void setStartConfig() {
		ENEMY_X=2*VECHICLE_WIDTH;
		ENEMY_Y=2*VECHICLE_HEIGTH;
		PLAYER_X=FRAME_WIDTH/2-(VECHICLE_WIDTH/2);
		 PLAYER_Y=FRAME_HEIGTH-VECHICLE_HEIGTH-36;
		 RIGHT_DISTANCE=FRAME_WIDTH-PLAYER_X; //odleg�o�c playera od prawej krawedzi
		SCALE_PLAYER=(double)RIGHT_DISTANCE/(double)FRAME_WIDTH;
		SCALE_ENEMY_Y=(double)ENEMY_Y/(double)FRAME_HEIGTH;
		FIRST_ENEMY=3; // (0-6) numer kolumny skrajnego lewego wroga
		LAST_ENEMY=3; //(0-6) numer kolumny skrajnego prawego wroga
		 SCALE_ENEMY=(double)ENEMY_X/(double)FRAME_WIDTH;
		BULLET_WIDTH=FRAME_WIDTH/200;
		 BULLET_HEIGTH=FRAME_HEIGTH/80;
		 FIRST_LINE=2*VECHICLE_HEIGTH;
		 COUNT_ENEMY=0;
		 MOVING_ENEMY_DOWN_TIME=500;
		 MOVING_ENEMY_LEVEL_TIME=9;
		
		 
	}
	/**
	 * Metoda sluzaca do uaktualniania danych po zmianie parametrow:
	 * @param x szerokosc okna
	 * @param y dlugsc okna
	 */
	public static void setConfig(int x, int y) {		
		FRAME_HEIGTH=y;
		FRAME_WIDTH=x;
		VECHICLE_WIDTH=x/GRID_X;
		VECHICLE_HEIGTH=y/GRID_Y;
		PLAYER_Y=FRAME_HEIGTH-VECHICLE_HEIGTH;
		ENEMY_Y=(int)(SCALE_ENEMY_Y*y);
		STEP_PLAYER=FRAME_WIDTH/50;
		RIGHT_DISTANCE=(int) (x*SCALE_PLAYER);
		setPlayerX(x-RIGHT_DISTANCE);	
		ENEMY_X=(int)(x*SCALE_ENEMY);
		BULLET_WIDTH=FRAME_WIDTH/200;
		BULLET_HEIGTH=FRAME_HEIGTH/80;
		BULLET_FROM_ENEMY_WIDTH=FRAME_WIDTH/20;
		BULLET_FROM_ENEMY_HEIGTH=FRAME_HEIGTH/20;
		 BULLET_X=(int)(SCALE_BULLET*FRAME_WIDTH);
		 BULLET_Y=(int)(SCALE_BULLET_Y*FRAME_HEIGTH);
		Game.setBulletsPositons();
		Game.setBulletsPositons_y();		
	}
	/**
	 * Metoda sluzaca pobraniu i przkonwertowaniu na jeden String Tablicy najlepszych wynikow
	 * @return Sting z najlepszymi wynikami
	 */
	public static String setBestScoresText() {
		String BestScores=null;
		BestScores="Best Scores:";
		for(int i=0;i<5;i++) {
			BestScores=(BestScores)+"\n";
			BestScores=((BestScores)+(String.valueOf(i+1)+"."+"\n")+("name: ")+(BEST_SCORES_NAMES[i])+("\n")+("points: ")+(BEST_SCORES[i]));
		}
		return BestScores;
	}

	public static void setPlayerX(int x) {
		PLAYER_X=x;		
	}
	public static void setSCALE_BULLET() {
		SCALE_BULLET=(double)BULLET_X/(double)FRAME_WIDTH;
		
	}
	public static void setFirstEnemy(int x) {
		FIRST_ENEMY=x;
	}
	public static void setLastEnemy(int x) {
		LAST_ENEMY=x;
	}
	public static void setEnemyX(int x) {
		ENEMY_X=x;
		SCALE_ENEMY=(double)ENEMY_X/(double)FRAME_WIDTH;
	}
	public static void setEnemyY(int y) {
		ENEMY_Y=y;
		
	}
	public int getBulletPositionX() {
		return BULLET_X;
	}
	public int getBulletPositionY() {
		return BULLET_Y;
	}
	public void setBulletPositionX(int x) {
		BULLET_X=x;
	}
	public void setBulletPositionY(int y) {
		BULLET_Y=y;
	}
	
	public static int getLevelsAmount() {
		return LEVELS_AMOUNT;
	}
	public static int[][][] getLEVELS() {
		return LEVELS;
	}
	public static void setLEVELS(int[][][] lEVELS) {
		LEVELS = lEVELS;
	}
	/**
	 * Metoda tworzaca tablice z uzupelnionymi (usatwionymi wrogami) juz levelami 
	 */
	public static void setLevels() {
		LEVELS=new int[LEVELS_AMOUNT][LINES][COLUMNS];
		for(int i=0 ;i<LEVELS_AMOUNT;i++) {		
			String temp=String.valueOf(1+i);
			LEVELS[i]=Config.setLevel("#LEVEL_"+temp);

		}
		
	}
	/**
	 * Metoda pobierajac instrukcje z pliku tekstowego
	 * @return
	 */
	public static String getInstruction() {
		File file = new File ("src//instruction.txt");
		Scanner in =null;
		try {
			in = new Scanner(file);
		}
		catch(FileNotFoundException e) {
			return null;
		}
		String instruction = null;
		try {
			instruction = ("\n"); 
			while(true) {
				instruction = instruction+in.nextLine()+("\n");
				if(instruction.equals("#END")) {
					break;
				}			
			}
		}
		catch(NoSuchElementException e) {
			try {
				in.close();
			}
			catch(IllegalStateException ise) {
				return null;
			}
			
		}
		catch (IllegalStateException e) {
			try {
				in.close();
			}
			catch(IllegalStateException ise) {
				return null;
			}
			
		}
		try {
			in.close();
		}
		catch(IllegalStateException ise) {
		}
		return instruction;
	}
	
	/**
	 * Metoda ustawia dla konkretnego levelu ustawienie wrogich pojazdow
	 *
	 * @param parameter nazwa levelu zgodnie z zapisem w config.txt
	 * 
	 *
	 * @return the int[][] zwraca dwuwymiarowa tablice z ustawieniem wrogich pojazdow w danym levelu
	 */
	public static int[][]setLevel(String parameter){
		String temp =fromFile(parameter);
		int count = LINES*COLUMNS;
		int[][] tempInt= new int [LINES][COLUMNS];
		int space_count=0;
		if(temp == null) {
			
			return defaultLevel;
		}
		try {
			int[] space = new int [count -1];
			space[0]= temp.indexOf(' '); // zwraca wartosc indeksu na ktorym znajduje sie spacja
			for (int i =0; i< count-2;i++) {
				space[i+1] = temp.indexOf(' ', space[i] +1); // zwraca wartosc indeksu na ktorym jest spacja ale po indeksie spac[i]+1
			}
			try {
				for(int i =0 ;i<LINES ;i++) {
					for(int j=0;j<COLUMNS; j++) {
						if(i==0&&j==0)tempInt[i][j]=(Integer.parseInt(temp,0,1,10));
						else if(i==3 && j==6)tempInt[i][j]=(Integer.parseInt(temp,54,55,10));
						else {
						tempInt[i][j]=(Integer.parseInt(temp,space[space_count]+1,space[space_count+1],10));
						space_count++;
						}
					}
					
				}
			}
			catch(NumberFormatException e) {
				
				return defaultLevel;
			}
		}
		catch(NullPointerException e) {
			
			return defaultLevel;
		}
		return tempInt;
	}
	
	/**
	 * Metoda pobierajaca konkretna dana z pliku config.txt
	 *
	 * @param parameter nazwa danej w config.txt
	 * 
	 * @return the int
	 */
	public static int setValue (String parameter) {
		String temp = fromFile(parameter);
		int tempValue;
		if(temp==null) return defaultValue;
		try {
			tempValue = Integer.parseInt(temp);
		}
		catch(NumberFormatException e) {
			return defaultValue;
		}
		return tempValue;
		
	}
	
	/**
	 * odczyt lini kodu odpowiednich parametrow z pliku config.txt
	 *
	 * @param description the description
	 * @return the string
	 */
	
	public static String fromFile(String description) {
		File file = new File ("src\\config.txt");
		Scanner in =null;
		try {
			in = new Scanner(file);
		}
		catch(FileNotFoundException e) {
			return null;
		}
		String parameter = null;
		try {
			while(true) {
				parameter = in.nextLine(); //przechodzi do nast�pnej lini kodu i zwraca zczytan�
				if(parameter.equals(description)) {
					parameter = in.nextLine(); // zapisuje do parameter linie kodu z danymi
					
					break;
				}
				
			}
		}
		catch(NoSuchElementException e) {
			try {
				in.close();
			}
			catch(IllegalStateException ise) {
				return null;
			}
			
		}
		catch (IllegalStateException e) {
			try {
				in.close();
			}
			catch(IllegalStateException ise) {
				return null;
			}
			
		}
		try {
			in.close();
		}
		catch(IllegalStateException ise) {
		}
		return parameter;

	}
	
}
