package Spaceinvaders;

import java.awt.Polygon;
import java.awt.Rectangle;
/**
 * Klasa (watke) sluzaca do poruszania sie pocisku gracza pionowo w gore
 * @author �ukasz Leszczy�ski
 *
 */
public class MovingBullet implements Runnable {
	Thread t;
	Enemy enemy [] =  new Enemy[Config.TOTALENEMY];
	public int bulletposition_x;
	public int bulletposition_y;
	public int count_enemy=0;
	public boolean bullet=false;
	MovingBullet(Game g, Enemy enemy[]){
		t =new Thread(this);
		this.enemy=enemy;
	
		t.start();			
			
		
	}
	public void run() {
		try {
			
			for(;;) {
					
				synchronized(this) {
					while(Config.isPAUSE()) {
						wait();
					}
					
				}
			
				if(Config.isPLAY()) {
					
					for(int i=0; i<Config.TOTALENEMY;i++) {
						if(enemy[i]!=null&&Config.isBULLET()) {
							int xpoints[]={enemy[i].getPosition_x(),enemy[i].getPosition_x()+Config.getVECHICLE_WIDTH()/2,enemy[i].getPosition_x()+Config.getVECHICLE_WIDTH()};
							int ypoints[]={enemy[i].getPosition_y()+(Config.getVECHICLE_HEIGTH()/2),enemy[i].getPosition_y()+Config.getVECHICLE_HEIGTH(),enemy[i].getPosition_y()+(Config.getVECHICLE_HEIGTH()/2)};
							if(new Polygon(xpoints,ypoints,3).intersects(new Rectangle(Config.getBULLET_X(),Config.getBULLET_Y(),Config.getBULLET_WIDTH(),Config.getBULLET_HEIGTH()))) {
								
								enemy[i].setHealth(enemy[i].getHealth()-Config.BULLET_DMG_PLAYER);
								
								if(enemy[i].getHealth()==0) {
									
									Config.setSCORES(Config.getSCORES()+enemy[i].getScore());
									enemy[i]=null;

									Config.setCOUNT_ENEMY(Config.getCOUNT_ENEMY()-1);
								}
							
								Config.setBULLET(false);
								
								
							}
						}		
					}

					Config.setBULLET_Y(Config.getBULLET_Y()-Config.getBULLET_WIDTH());
					Config.setSCALE_BULLET_Y();
					if(Config.getBULLET_Y()<0) {
						Config.setBULLET(false);
						
					}
				}

				Thread.sleep(10);
				
				
			}

			}
		catch(InterruptedException e) {
			
		}
		
		                 
	}
synchronized void myresume() {
		
		
		notifyAll();
	}
}
