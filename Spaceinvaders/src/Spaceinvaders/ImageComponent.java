package Spaceinvaders;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

public class ImageComponent extends JComponent{
	
		private Image player_image;
		private Image enemy_1_image;
		private Image enemy_2_image;
		private Image enemy_3_image;
		private Image bullet_1_image;
		private Image bullet_2_image;
		
		
		public ImageComponent() {
			 player_image= new ImageIcon(this.getClass().getResource("player.png")).getImage();
			 enemy_1_image= new ImageIcon(this.getClass().getResource("enemy_1.png")).getImage();
			 enemy_2_image= new ImageIcon(this.getClass().getResource("enemy_2.png")).getImage();
			 enemy_3_image= new ImageIcon(this.getClass().getResource("enemy_3.png")).getImage();
			 bullet_1_image=new ImageIcon(this.getClass().getResource("bullet_1.png")).getImage();
			 bullet_2_image=new ImageIcon(this.getClass().getResource("bullet_2.png")).getImage();
		}
		/*
		public void paintPlayer(Graphics player,int playerposition_x ) {
			player.drawImage(player_image, playerposition_x,Config.getFRAME_HEIGTH()-Config.getVECHICLE_HEIGTH(),null);
			
			
		}
		public void paintBullet_1(Graphics bullet_1,int bulletposition_x , int bulletposition_y) {
			bullet_1.drawImage(bullet_1_image, bulletposition_x,bulletposition_y,null);
		}
		
		public void paintBullet_2(Graphics bullet_2,int bulletposition_x , int bulletposition_y) {
			bullet_2.drawImage(bullet_2_image, bulletposition_x,bulletposition_y,null);
		}
		public void paintEnemy_1(Graphics enemy_1,int position_x, int position_y) {
			enemy_1.drawImage(enemy_1_image, position_x,position_y,null);
			
			
		}
		public void paintEnemy_2(Graphics enemy_2,int position_x, int position_y) {
			enemy_2.drawImage(enemy_2_image, position_x,position_y,null);
			
			
		}
		public void paintEnemy_3(Graphics enemy_3,int position_x, int position_y) {
			enemy_3.drawImage(enemy_3_image, position_x,position_y,null);
			
			
		}
		*/
		public Image getImage(int i) {
			/*switch(i) {
			case 1:
				return enemy_1_image;
				break;
				
			case 2:
				return enemy_2_image;
				
			case 3:
				return enemy_3_image;
			case 0:
				return player_image;
			case 4:
				return bullet_1_image;
			case 5:
				return bullet_2_image;
			default:
				System.out.println("Error");
				
			}
			*/
			
			if (i==1) return enemy_1_image;
			else if(i==2) return enemy_2_image;
			else if(i==3) return enemy_3_image;
			else if(i==0)return player_image;
			else if(i==4)return bullet_1_image;
			else return bullet_2_image;
			
		}	
}
