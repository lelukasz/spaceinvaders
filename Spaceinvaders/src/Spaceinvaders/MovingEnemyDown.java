package Spaceinvaders;
/**
 * Klasa (watke) sluzaca do poruszania sie wrogich pojazdow pionowo w dol
 * @author �ukasz Leszczy�ski
 *
 */
public class MovingEnemyDown implements Runnable{
	Thread t;
	Enemy enemy [] =  new Enemy[Config.TOTALENEMY];
	MovingEnemyDown(Game g, Enemy enemy[]){
		this.enemy=enemy;
		t=new Thread (this);
		t.start();
	}
	
	public void run() {
		try {
			for(;;) {
				synchronized(this) {
					while(Config.isPAUSE()) {
						wait();
					}
					
				}
				moveDown();

				if((Config.getDOWN_ENEMY_Y()+Config.getVECHICLE_HEIGTH())>=Config.getPLAYER_Y()) {
					Config.setEND_GAME(true);
				}

				Thread.sleep(Config.getMOVING_ENEMY_DOWN_TIME());
			}
		}
		catch(InterruptedException e) {
			
		}
	}
	public void moveDown() {
		for( int i=0;i<Config.TOTALENEMY;i++) {
			if(enemy[i]!=null) {			
				enemy[i].setPosition_y((enemy[i].getPosition_y())-1);
				
			}
			
		}
		Config.setENEMY_Y(Config.getENEMY_Y()+1);
		Config.setSCALE_ENEMY_Y();

	}
	synchronized void myresume() {
		notifyAll();
	}
}


