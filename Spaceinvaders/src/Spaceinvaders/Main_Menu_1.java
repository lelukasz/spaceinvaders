package Spaceinvaders;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;



/**
 * Kalsa z menu glownym
 * @author �ukasz Leszczy�ski
 *
 */
public class Main_Menu_1 extends JFrame implements ActionListener {
	int[][] placing;

	JButton button_play, button_instruction,button_best_scores;
	static GameFrame game;

	public Main_Menu_1(){

		setSize(400,600);
		setTitle("Space Invaders Main Menu");
		setLayout(null);
		
		button_play = new JButton("Start Game");
		button_play.setBounds(150, 100, 100, 20);
		add (button_play);
		button_play.addActionListener(this);
		
		button_instruction = new JButton("Instruction");
		button_instruction.setBounds(150, 150, 100, 20);
		add(button_instruction);
		button_instruction.addActionListener(this);
		
		button_best_scores = new JButton("Best Scores");
		button_best_scores.setBounds(150, 200, 100, 20);
		add (button_best_scores);
		button_best_scores.addActionListener(this);
		
		
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source == button_play) {
			 game = new GameFrame();
			this.setVisible(false);
		}
		else if( source == button_instruction) {
			Config instruction=new Config();
			JOptionPane.showMessageDialog(this, instruction.getInstruction());
		}
		else if( source == button_best_scores) {
			
			JOptionPane.showMessageDialog(this, Config.setBestScoresText());
		}
		else
		System.out.println("elo mordo");
		
	}
static void closeMain() {
		
	
	}
	
}
