package Spaceinvaders;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.*;

/**
 * Klasa tworzaca ramke do gry
 * @author �ukasz Leszczy�ski
 *
 */
public class GameFrame extends JFrame {
	
	Game game1;

	GameFrame(){	
		setSize(Config.getFRAME_WIDTH(),Config.getFRAME_HEIGTH());
		setBounds(0,0,Config.getFRAME_WIDTH(),Config.getFRAME_HEIGTH());
		setTitle("SpaceInvaders");
		setResizable(true);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.black);
		
		
		BestScores bestScores=new BestScores ();
		if(Config.isEND_GAME()) {
			add(bestScores);
		}
		else {
			game1=new Game();
			add(game1);
		}
		
		getContentPane().addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				Component c = (Component)e.getSource();
				
				Config.setConfig(c.getWidth(),c.getHeight());
			}
		});
	
	}
	
	
}
